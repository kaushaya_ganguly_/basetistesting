# Overview

To build a plugin from the source code, we would manually have to import the source code on eclipse, select `export` > `Plugin Project`. 

However, for true automation testing, this is a lot of manual work. For this reason, we build the plugins automatically too. These plugins maybe the target plugin which is to be tested, or dependency plugins for the target application. Either way, this helps you generate the plugin directly from the source code without having to manually import and export as plugins on eclipse.

We generate the plugins using Maven and Tycho (which is just a Maven Plugin). The information about how to configure has been described here.

The steps are:

	1. Download the source code from SVN to jenkins.
	2. As a post-build step, generate poms using maven tycho plugin and edit the parent pom.xml a bit.
	4. Now simply build it in jenkins.

---------------------------------------------------------------
# Create New Maven Project

Since the plugins are built using Maven, you need to create a new maven project and configure it accordingly.

On the Jenkins dashboard, go to `New Item` on the left corner , select `Maven project` , give it a name and click `OK`.

Immedeiately after, you'll be redirected to the `Configure` page of the project you just created. Here we need to configure some stuff before we can get started.

## Source Code Management

In this testing, we choose to get the latest commit of the source code from the SVN repository. We download the source code of the plugins we need to generate (this can be the plugins to be tested, or the dependency plugin for the target plugin).

At the `Source Code Management` section, select `Subversion Modules` and add the URL of the project. Immediately after, you will be prompted to add the proper credentials in another tab. After you've added the credentials, you can select it from `Credentials`. Add the `Local Module Directory` and click `Save`.

<h4>SVN repository</h4>

The eclipse source code for the eclipse plugins that we've tested in this project can be found in the following SVN repository:


	Link: https://subversion.assembla.com/svn/basetis-java
	Username : basetis.java
	password: [Contact the administrator]

(Please make sure you've selected `ignore externals`)

<h4>Projects</h4>

We built the following projects in the folder <b>code-generator-emf</b>:

	Engine-xtend: tag version 2.3.0
	Model-ecore: the three projetcs with tag version 1.2.0
	Ada-workbenc: tag version 1.1.0
	Ada-sample-app/.OLD/model-ecore-1.2.0s

Finally, your Source Code Management View should look like this : 

(Note: This is only one project url. Add as many as you need, they'll look the same)


![Screenshot](images/svnrepository.png)

##Intermediate steps:

* Build the project with `Build Now` from the left corner, so that jenkins downloads all the repository you added.
* Now in the terminal/cmd , cd into the root directory where your jenkins project is, (in this case it's `$HOME/.jenkins/jobs/AdaWorkbenchBuild` ). For more information, refer below to `Generating parent and child pom.xml`

### Generating parent and child pom.xml

Run the following code from the terminal to generate the parent and the child pom.xml.

	mvn org.eclipse.tycho:tycho-pomgenerator-plugin:generate-poms -DgroupId=com.basetis.ada

Please note that you should be in the root directory where you want the parent pom.xml. For example, if your directory structure looks like this:

	parent/
		sourceCodePlugin1
		sourceCodePlugin2
	
Then after generating the parent and the child pom.xml, your directory structure should look like this:

	parent/
		sourceCodePlugin1/
			pom.xml                #Child pom.xml 1
			.....
		sourceCodePlugin2/
			pom.xml          	   #Child pom.xml 2
			.....
		pom.xml       			   #Parent pom.xml


However, we need to edit the default `parent pom.xml` just once. Refer to the following code:

	<?xml version="1.0" encoding="UTF-8"?>
	<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  	<modelVersion>4.0.0</modelVersion>
  	<groupId>com.basetis.ada</groupId>
  	<artifactId>workspace</artifactId>
  	<version>0.0.1-SNAPSHOT</version>
  	<packaging>pom</packaging>
  	<modules>
    <module>com.basetis.codegen.model.ecore</module>
    <module>com.basetis.codegen.engine.xtend.ada</module>
    <module>com.basetis.workbench.ada</module>
 	 </modules>
    <properties>
        <tycho.version>0.23.0</tycho.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>  
        <mars-repo.url>http://download.eclipse.org/releases/mars</mars-repo.url>
        
    </properties>

    <repositories>
        <repository>
            <id>mars</id>
            <url>${mars-repo.url}</url>
            <layout>p2</layout>
        </repository>

    </repositories>

    <build>
        <plugins>
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-maven-plugin</artifactId>
                <version>${tycho.version}</version>
                <extensions>true</extensions>
            </plugin>

            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>target-platform-configuration</artifactId>
                <version>${tycho.version}</version>
            </plugin>
	    <plugin>
	      <groupId>org.eclipse.xtend</groupId>
	      <artifactId>xtend-maven-plugin</artifactId>
	      <version>2.7.3</version>
	      <executions>
		<execution>
		  <goals>
		    <goal>compile</goal>
		    <goal>testCompile</goal>
		  </goals>
		  <configuration>
		    <outputDirectory>xtend-gen/</outputDirectory>
		    <!--<testOutputDirectory>${project.build.directory}/xtend-gen/test</testOutputDirectory>-->
		  </configuration>
		</execution>
	      </executions>
	    </plugin>
        </plugins>
    </build>
</project>
## Building from pom.xml

Now go back to the maven project configuration page in jenkins. Under `Build`, configure Maven with the following details:

	Maven Version: Select from dropdown list
	Root POM: The parent POM relative path
	Goals and options: clean verify

Finally, it should look like this:

![Screenshot](images/mvnbuild.png)

## Post Steps

Under Post Steps, we need to copy the generated plugin from the build process to the plugins folder of the target eclipse.

*(Please note: You can choose to do this directly from Maven too, but for now, a simpler way has been implemented)*

Select `Execute shell` from the dropdown list and add the following code:

	ls sudo find . -iwholename "*/target/*.jar" -exec cp {} $HOME/../../../../home/datalab/eclipse/plugins/ \;

Please change the code to the appropriate absolute path of the target eclipse.

# Final Note

Now you've successfully set up the build process! After this, you simply need to `Build Now` from jenkins everytime you need to build it.
# Introduction

This documentation covers the basics of how to build and reproduce the test automation suite we built here at Basetis!

	The tool(s) we use for :

		1. Continuous Integration		=		Jenkins	
		2. Automatic build				=	 	Maven (with Tycho) and Ant (depreciated for this purpose)
		3. Functional Testing 			= 		RCPTT


<h4>What is being done here really?</h4>

1. **Building plugins** :<br>
	* Plugins to be tested
	* Plugins to be used as dependencies for target plugins or RCP applications.
2. **Creating tests for RCP applications**
	* Recording tests
	* GUI tests and functional testing
	* Replaying tests at regular intervals
3. **Automatically and periodically building and running these from a jenkins server**
	* Start building without having to launch eclipse, build or replay tests.



## Which steps and what has been automated?

<h4>Manual build of Plugins</h4>

In reality, we need to abide by the following manual steps to build the plugins.

* You need to `start eclipse` > `Select project` > `Right Click` > `Export` > where you have to select `Deplyable plug-ins and fragments` .. 

![Screenshot](images/manual1.png)

**BUT NOW**, we just have one step to build everything:

![Screenshot](images/build2.png)![Screenshot](images/correct.png)


<h4>Launch eclipse manually for every test</h4>

# Technical Overview

Here you can find a little overview about the technical aspect of the automation system. The tools for different purposes have already been mentioned in the beginning of this page. Here, you can find a little more details about each tool and why we chose this.

### Continuous Integration - Jenkins

The following are the basic functionalities associated with jenkins:

* Automatically fetch the updated code from subversion.
* Builds the plugins automatically.
* Launches Eclipse automatically for testing the RCP applications.
* Configure to build/run tests any time - daily/weekly/monthly/yearly.
* You get detailed reports to your email if case any build failed.
* You can avoid all of the manual work associated with building, testing and running these tests.

### Automatic Building - Maven (with Tycho)

Most of the automatic building process has been done using Maven. The `pom.xml` is where we configure all the maven jobs. 

### Automatic Building - Ant

Ant is simple and gives you more control over the workflow. However, after building using both Ant and Maven, we decided to go with Maven because Ant is not innately configured to build plugins, whereas Maven is. We configured Ant Manually to generate plugins, but it's not the natural behavior. For any further use, we've kept Ant as an option.

###  GUI and Function testing - RCPTT

The testing tool we use for our RCP applications is RCPTT. We've tried and tested various other testing tools like Jubula. However, RCPTT is simple and efficient and is serves our purpose here. 

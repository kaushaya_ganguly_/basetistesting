**Please Note: This assumes you already have Jenkins installed on your system, or you have the proper credentials to access a live jenkins url. Either way, this documentation helps you start from scratch, once you have access to jenkins either locally, or on a server.**


---------------------------------

# Jenkins Global Configuration

There are some configurations we need before we can start building and testing our eclipse plugins. To change the jenkins configuration, on the jenkins dashboard, go to 

`Configure Jenkins` > `Configure System`.

## Change the default Jenkins Home Directory

If the default Jenkins Home Directory on your system is `/var/lib/jenkins`. This creates a lot of permission issues when you try to move around in your file system via jenkins, or during any debugging steps. Either way, it's always better to change the default jenkins home directory to a public directory like `$HOME` or `$HOME/desktop`.

For more information about how to change the default Home Directory on your particular system, please search online. It's pretty simple.

## Configure JDK

If you have JDK installed on your system, then you can add it manually to jenkins by clicking on `Add JDK` and proving the name of the JDK version and add the local path to `JAVA_HOME`. 

![Screenshot](images/java.png)

If you don't have Java installed, you can also select `Install automatically` and jenkins will download it for you after you follow the required steps.

## Configure Maven

To configure maven, click on `Add Maven` and add the local maven installation path if you have it installed on your local system.
Else, you can also select `Install automatically` from Apache. As of now November 2015, it's maven v. 3.3.3.

![Screenshot](images/maven.png)
<h1>Quick Start</h1>

----------------------------------------------------------------------------------------------------------------------------------------------------------

This is a quick start to Automatically testing the eclipse plugins/products without having to manually launch eclipse, add dependencies, manage directories, updating code from subversion, etc. This test automation suite does everything for you.

We use Jenkins to launch the test automation suite. To Launch the test, follow the following steps in the <b>same order</b> as given below:

# Launch Jenkins
* Go to [192.168.1.126:8081](http://192.168.1.126:8081/).

![Screenshot](images/link.png)

# Build the Plugins

* Click on the process with the name "<b>AdaWorkbenchBuild</b>"

![Screenshot](images/build.png)

and start the process by clicking "Build Now" on the left hand side....

![Screenshot](images/build2.png)

* To see the progress of the build, click on the #(Latest Build Number) under "Build History" on the left.

![Screenshot](images/consoleoutput.png)


# Test the Plugin

* Now to test the generated plugin, go to the Jenkins dashboard and click on the testing project available.

![Screenshot](images/testingsvn.png)

and start it...

![Screenshot](images/testingbuild.png)
# Overview

The test cases are written using RCPTT plugin in eclipse. It captures the UI elements, records the actions we prefer to test, and replays them to test if the process is successful or not under different circumstances. We have tried various other testing plugins in eclipse and we decided to go with RCPTT for its simplicity and stable nature.

After the test case has been created using RCPTT, we save this test case in the form of a RCPTT test project (similar to a normal java project) and commit it on the subversion. This code is then invoked by jenkins and is triggered by jenkins so that we don't need to manually replay the test cases.

The steps are:

	1. Create the RCPTT test project in Eclipse.
	2. Create the pom.xml for this project in eclipse.
	3. Upload this project to SVN.
	4. Download this project on jenkins and build it from there itself.


--------------------------------------------------------------

# RCPTT eclipse plugin

To create the test cases, we use the RCPTT plugin on eclipse. To install RCPTT plugin in eclipse, fire up eclipse, go to `Help`>`Eclipse Marketplace` and search for the plugin RCPTT and install.

For more information about how to create test cases using RCPTT plugin in eclipse, please refer to [the official documentation of RCPTT](https://www.eclipse.org/rcptt/documentation/userguide/getstarted/)

After you've learned how to use RCPTT, record different element actions and save the test project. We still have to create the pom.xml so that you don't need to `replay` manually , but run it directly from jenkins.

# Create pom.xml

To run this test automatically from jenkins, we need Maven. For this reason we need to configure the pom.xml.

The pom.xml being used here is as follows:

	<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		     xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>
	  <!-- Set artifact id according to a RCPTT project name  -->
	  <artifactId>AdaWorkbench</artifactId>
	  <!-- Set group id based on application under test name -->
	  <groupId>com.basetis.ada</groupId>
	  <!-- <version>2.0.1</version>-->
	  <version>1.0.0</version>
	  <!-- RCPTT Maven Plugin provides this packaging type -->
	  <packaging>rcpttTest</packaging>

	  <!-- RCPTT Maven Plugin and RCPTT Runner are hosted in this repository -->
	  <pluginRepositories>
	    <pluginRepository>
	      <id>rcptt-releases</id>
	      <name>RCPTT Maven repository</name>
	      <url>https://repo.eclipse.org/content/repositories/rcptt-releases/</url>
	    </pluginRepository>
	    <pluginRepository>
	      <id>rcptt-snapshots</id>
	      <name>RCPTT Maven Snapshots repository</name>
	      <snapshots>
	        <updatePolicy>always</updatePolicy>
	      </snapshots>
	      <url>https://repo.eclipse.org/content/repositories/rcptt-snapshots/</url>
	    </pluginRepository>
	  </pluginRepositories>

	  <!-- 
	       If RCPTT tests are planned to be run on an update site, 
	       RCPTT Maven Plugin can download Eclipse SDK of required version
	       for current platform from repository below (to see 
	       an up-to-date list of available versions visit this link:
	       http://maven.xored.com/nexus/content/repositories/ci4rcptt-releases/org/eclipse/sdk/
	  -->
	  <properties>
	    <rcptt-maven-version>2.1.0-SNAPSHOT</rcptt-maven-version>
	  </properties>

	  <!-- The main part of RCPTT Maven plugin -->
	  <build>
	    <plugins>
	      <plugin>
	        <groupId>org.eclipse.rcptt</groupId>
	        <artifactId>rcptt-maven-plugin</artifactId>
	        <version>${rcptt-maven-version}</version>
	        <extensions>true</extensions>
	        <configuration>

	          <!-- This element describes where to get an AUT to run tests -->
	          <aut>
	            <!-- There are several ways to specify AUT location,
	                 Uncomment an element corresponding to a most suitable way
	            -->

	            <!-- Use AUT from current build results. [classifier] will be
	            automatically replaced according to the current platform -->
	            <!--
	            <explicit>${project.basedir}/../product/target/products/org.eclipse.ui.examples.rcp.browser.product-[classifier].zip</explicit>
	            -->

	            <!-- Or specify a path to AUT folder -->
	            
	            <explicit>C:\eclipse-rcp-luna-SR2-win32-x86_64\eclipse</explicit>


	            <!-- As well AUT can be downloaded from HTTP -->
	            <!--
	            <explicit>http://server/path/aut-archive.zip</explicit>
	            -->
	            
	            <!-- AUT can be downloaded automatically from Maven repository -->
	            <!-- 
	            <groupId>com.company.product</groupId>
	            <artifactId>com.company.product.rcp</artifactId>
	            <version>3.7.2</version>
	            -->

	            <!-- AUT Artifact classifier is based on a current platform:
	                 Windows 32bit: win32.win32.x86
	                 Windows 64bit: win32.win32.x86_64
	                 Linux 32bit: linux.gtk.x86
	                 Linux 64bit: linux.gtk.x86_64
	                 Mac OS X 64bit: macosx.coca.x86_64
	            -->

	            <!-- Optionally it is possible to specify extra features to 
	                 be installed into AUT, this is useful when RCPTT tests
	                 need to be executed using a repository assembled as 
	                 part of current Maven build.

	                 Inside an 'injection element it is possible to specify a list of 
	                 features/plugins to install, if nothing is set, all available
	                 features will be installed.
	                 feature.group suffix is required by p2,
	                 otherwise it searches for a plugin with given id

	                 Currently RCPTT Runner does not install requirements automatically,
	                 so the full list of features to install must be explicitly set
	            -->


	                 
	            <injections>
	              <!-- features are optional - when omitted, all features from given site will be installed -->
	              <!--
	              <injection>
	                <site></site>
	                <features>
	                  <feature>com.comanyname.featureid</feature>
	                </features>
	              </injection>
	              -->
	            </injections>

	           <!-- additional AUT args (besides taken in its ini-file) can be set 
	                Use one argument per <arg> element -->
	           <!-- 
	            <args>
	              <arg>-ws</arg>
	              <arg>$${target.ws}</arg>
	            </args>
	            -->
	           <!-- optional VM args can be set too -->
	           <!--
			   <vmArgs>
				 <vmArg>-Xmx768m</vmArg>
				 <vmArg>-XX:MaxPermSize=256m</vmArg>
			   </vmArgs>
	           -->
	          </aut>

	          <runner>
	            <!-- RCPTT Runner location can be set using the same methods 
	                 as AUT location:
	            -->
	            <!--
	            <explicit>/path/to/rcptt/runner</explicit>
	            -->
	            
	            <!-- but the most convenient way is to just set its version,
	                 RCPTT maven plugin will automatically set right groupId and
	                 artifact ID and will download RCPTT Runner from Xored Maven Repo -->
	            <version>${rcptt-maven-version}</version>

	            <vmArgs>
	              <vmArg>-Xmx1024m</vmArg>
	              <vmArg>-XX:MaxPermSize=256m</vmArg>
	            </vmArgs>
	          </runner>

	          <!-- Test options for RCPTT Runner, most popular options listed here.
	               Full list of options is available at:
	               https://ci.xored.com/doc/runner/
	          -->
	          <testOptions>
	            <!-- Timeout for all tests, in seconds -->
	            <execTimeout>1800</execTimeout>
	            <!-- Timeout for a single test case, in seconds -->
	            <testExecTimeout>300</testExecTimeout>

	            <!-- When set to true, in case of test failure
	                 AUT will be restarted. This significantly
	                 slows down execution, but may be useful 
	                 for some test suites -->
	            <!--
	            <restartAUTOnFailure>true</restartAUTOnFailure>
	            -->
	          </testOptions>

	          <!-- By default RCPTT Runner runs tests from a project directory,
	               but in some cases it might be required to import additional 
	               projects into runner's workspace -->
	          <!--
	          <projects>
	            <project>${project.basedir}/../project</project>
	          </projects>
	          -->

	          <!-- By default RCPTT Runner runs all tests from workspace,
	               but it is possible to pass test suite names, so it
	               will execute only test suites from given project. -->

	          <!--
			  <suites>
				<suite>MyTestSuite</suite>
			  </suites>
	          -->

	          <!-- Sometimes it might be useful to skip a test case
	               (for instance because of some unresolved bug). RCPTT
	               can skip tests based on its tags. By default RCPTT skips
	               tests with tag 'skipExecution' (this value has been 
	               chosen because on one hand it is descriptive enough,
	               on another hand it is unlikely that this tag name
	               will collide with some user's tag)
	               -->
	          <!--
	          <skipTags>
	            <skipTag>linuxOnly</skipTag>
	          </skipTags>
	          -->

	          <!-- By default RCPTT generates a single HTML report file with
	               all tests, but it is possible to generate one file per
	               test -->
	          <!--
	          <splitHtmlReport>true</splitHtmlReport>
	          -->
	        </configuration>
	      </plugin>
	    </plugins>
	  </build>

	</project>

*Note- you have to change the path to the target eclipse plugin that you'll be using. For example if my target eclipse is in `$HOME/desktop/eclipse`, then I should edit the path inside `<excplicit>` with the correct path. Also, you must change the lines where they mention eclipse "mars" to the eclipse version you are using*.mak

## Save test project to subversion

The test project created with RCPTT on eclipse, should be saved in the subversion so that it can be accessed by Jenkins. Once Jenkins has access to this code, it can run it automatically, resulting in launching eclipse as the AUT (Application Under Test) and running the recored tests automatically.

*(Please note: You should create a new project on subversion, say com.basetis.ada.workbench.testing and call this from jenkins)*

## Generate test from jenkins

In Jenkins, create a `New Item` > `Freestyle project`. Give it a name and click `OK`. This will redirect you to the `configure` page of the project.

* Under `Source Code Management` you must fetch the test project from subversion by selecting `Subversion` and providing the repository URL of the subversion RCPTT test project you created using eclipse.


	![Screenshot](images/rcpttsvn.png)


* Under `Build`, select `Invoke top-level Maven targets` from the dropdown list and select the version of Maven you are using (preferably 3.3.3). For goals, you must specify `clean package`.

	![Screenshot](images/mavenrcptt.png)

After this you're good to go! Just `Save` the changes, then click on `Build now` on the left side. 